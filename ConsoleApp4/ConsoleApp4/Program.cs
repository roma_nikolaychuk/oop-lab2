﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.WriteLine("Лабораторна робота №2.\nВиконав: Ніколайчук Р.О., група СН-21\nВаріант №9\nЗавдання №3.\n");
            int n = 25;
            int[] numbers = new int[n];
            int PositiveElements_kilkist = 0;
            int NegativeElements_kilkist = 0;
            int NumberOfPairedItems = 0;
            int NumberOfOddElements = 0;

            for (int i = 0; i < n; i++)
            {
                numbers[i] = Convert.ToInt32(Console.ReadLine());
                if (numbers[i] == 0)
                {
                    for (i = 0; i < n; i++)
                    {
                        if ((numbers[i] % 2) != 0)
                        {
                            NumberOfOddElements++;
                        }
                        else
                        {
                            NumberOfPairedItems++;
                        }
                    }
                    for (i = 0; i < n; i++)
                    {
                        if (numbers[i] > 0)
                            PositiveElements_kilkist++;
                        if (numbers[i] < 0)
                            NegativeElements_kilkist++; 
                    }
                }
            }
            Console.WriteLine("Кількість додатніх елементів - {0}", PositiveElements_kilkist);
            Console.WriteLine("Кількість від'ємних елементів - {0}", NegativeElements_kilkist);
            Console.WriteLine("Кількість непарних елементів - {0}", NumberOfOddElements);
            Console.WriteLine("Кількість парних елементів - {0}", NumberOfOddElements);
            Console.ReadKey();
        }
    }
}