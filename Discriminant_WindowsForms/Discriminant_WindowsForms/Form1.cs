﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discriminant_WindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            textBox7.Visible = false;
            label9.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
                int a = int.Parse(textBox1.Text);
                int b = int.Parse(textBox2.Text);
                int c = int.Parse(textBox3.Text);

            do
            {
                
                if (a == 0)
                {
                    MessageBox.Show("Помилка  введення  значення 'a', 'a' має бути > 0!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (a < 0)
                {
                    MessageBox.Show("'a' не може бути < 0!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                  
                    int D = b * b - 4 * a * c;
                    double x1, x2;
                    x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    x2 = (-b - Math.Sqrt(D)) / (2 * a);
                    textBox4.Text = D.ToString();

                    if (D > 0)
                    {
                        textBox7.Visible = false;
                        label9.Visible = false;

                        textBox6.Visible = true;
                        textBox5.Visible = true;
                        label6.Visible = true;
                        label7.Visible = true;
                        textBox6.Text = x1.ToString("F2");
                        textBox5.Text = x2.ToString("F2");
                        return;
                    }
                    else if (D < 0)
                    {
                        textBox5.Visible = false;
                        textBox6.Visible = false;
                        label6.Visible = false;
                        label7.Visible = false;
                        label9.Visible = false;
                        textBox7.Visible = false;
                        MessageBox.Show("Рівняння немає коренів!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        textBox5.Visible = false;
                        textBox6.Visible = false;
                        label6.Visible = false;
                        label7.Visible = false;
                        double x = x1 = x2;
                        label9.Visible = true;
                        textBox7.Visible = true;
                        textBox7.Text = x.ToString("F2");
                        return;
                    }
                }
            } while (true);

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }
    }
}
    