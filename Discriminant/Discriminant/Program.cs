﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discriminant
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.WriteLine("\tАвтоматичне розв'язання квадратного рівняння, знаходження значення дискримінанта та його коренів");
            Console.WriteLine("Вимоги розв'язку квадратного рівняння: a > 0 - рівняння матиме розв'язок\n");
            do {
                Console.Write("Введіть a = ");
                int a = Convert.ToInt32(Console.ReadLine());

                if (a == 0)
                {
                    Console.WriteLine("Помилка введення 'а', будь ласка введіть а > 0");
                }
                else if (a < 0)
                {
                    Console.WriteLine("a не може бути < 0!");
                }
                else
                {
                    Console.Write("Введіть b = ");
                    int b = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Введіть c = ");
                    int c = Convert.ToInt32(Console.ReadLine());
                    int D = b * b - 4 * a * c;
                    double x1, x2;
                    x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    x2 = (-b - Math.Sqrt(D)) / (2 * a);
                    Console.WriteLine("\nДискримінант D = {0}\n", D);

                    if (D > 0)
                    {
                        Console.WriteLine("Корені:\nx1 = {0}\nx2 = {1}", x1, x2);
                        Console.ReadKey();
                        return;
                    }
                    else if (D < 0)
                    {
                        Console.WriteLine("Рівняння немає коренів!");
                        Console.ReadKey();
                        return;
                    }
                    else
                    {
                        double x = x1 = x2;
                        Console.WriteLine("Рівняння має один корінь:\nx = {0}", x);
                        Console.ReadKey();
                        return;
                    }
                }
            } while (true);
        }
    }
}
