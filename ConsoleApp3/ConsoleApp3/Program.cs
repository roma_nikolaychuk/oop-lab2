﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.WriteLine("Лабораторна робота №2.\nВиконав: Ніколайчук Р.О., група СН-21\nЗавдання №3.\n");

            int N;
            int K;
            int Suma = 0;

            do
            {
                Console.Write("Введіть ціле число N = ");
                if ((!int.TryParse(Console.ReadLine(), out N)) || N <= 0)
                    Console.WriteLine("Помилка введеня значення N! Будь ласка введіть ціле число!");
                else
                    break;
            } while (true);

            do
            {
                Console.Write("Введіть ціле число K = ");
                if ((!int.TryParse(Console.ReadLine(), out K)) || K <= 0)
                    Console.WriteLine("Помилка введеня значення K! Будь ласка введіть ціле число!");
                else
                    break;
            } while (true);

            for (int i = 1; i <= N; i++)
            {
                Suma += Convert.ToInt32(Math.Pow(i, K));
            }
            Console.WriteLine("\nСума = {0}", Suma);
            Console.ReadKey();
        }
    }
}
