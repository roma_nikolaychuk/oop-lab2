﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.WriteLine("Лабораторна робота №2.\nВиконав: Ніколайчук Р.О., група СН-21\nВаріант №9\nЗавдання №1.");

            double x;
            do
            {
                Console.Write("Введіть дробове значення x = ");
                if (!double.TryParse(Console.ReadLine(), out x))
                    Console.WriteLine("Помилка введеня значення x! Будь ласка повторіть введене значення ще раз!");
                else
                    break;
            } while (true);

            double y;
            do
            {
                Console.Write("Введіть дробове значення y = ");
                if (!double.TryParse(Console.ReadLine(), out y))
                    Console.WriteLine("Помилка введеня значення y! Будь ласка повторіть введене значення ще раз!");
                else
                    break;
            } while (true);

            double z;
            do
            {
                Console.Write("Введіть дробове значення z = ");
                if (!double.TryParse(Console.ReadLine(), out z))
                    Console.WriteLine("Помилка введеня значення z! Будь ласка повторіть введене значення ще раз!");
                else
                    break;
            } while (true);

            double s;
            s = Math.Abs(Math.Pow(x, y / x) - Math.Pow(y / x, 1.0 / 3.0)) + (y - x) * (Math.Cos(y) - (z) / (y - x)) / (1 + Math.Pow(y - x, 2));
            Console.Write($"Результат обчислення: s = ");
            Console.WriteLine("{0:0.###}", s);

            Console.ReadKey();
        }
    }
}