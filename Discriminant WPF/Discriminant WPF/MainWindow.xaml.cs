﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Discriminant_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int a = int.Parse(TextBox_a.Text);
            int b = int.Parse(TextBox_b.Text);
            int c = int.Parse(TextBox_c.Text);
            do
            {
                if (a == 0)
                {
                    TextBox_x.Visibility = Visibility.Hidden;
                    TextBox_x1.Visibility = Visibility.Hidden;
                    TextBox_x2.Visibility = Visibility.Hidden;
                    label_x.Visibility = Visibility.Hidden;
                    label_x1.Visibility = Visibility.Hidden;
                    label_x2.Visibility = Visibility.Hidden;
                    MessageBox.Show("Помилка  введення  a!\na має бути > 0 ", "Помилка", MessageBoxButton.OK,MessageBoxImage.Error);
                    return;
                }
                else if (a < 0)
                {
                    TextBox_x.Visibility = Visibility.Hidden;
                    TextBox_x1.Visibility = Visibility.Hidden;
                    TextBox_x2.Visibility = Visibility.Hidden;
                    label_x.Visibility = Visibility.Hidden;
                    label_x1.Visibility = Visibility.Hidden;
                    label_x2.Visibility = Visibility.Hidden;
                    MessageBox.Show("a не може бути < 0", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                else
                {
                    int D = b * b - 4 * a * c;
                    double x1, x2;
                    x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    x2 = (-b - Math.Sqrt(D)) / (2 * a);
                    

                    if (D > 0)
                    {
                        TextBox_x.Visibility = Visibility.Hidden;
                        label_x.Visibility = Visibility.Hidden;
                        label_x1.Visibility = Visibility.Visible;
                        label_x2.Visibility = Visibility.Visible;
                        TextBox_x1.Visibility = Visibility.Visible;
                        TextBox_x2.Visibility = Visibility.Visible;
                        TextBox_D.Text = D.ToString("F2");
                        TextBox_x1.Text = x1.ToString("F2");
                        TextBox_x2.Text = x2.ToString("F2");
                        return;
                    }

                    else if (D < 0)
                    {
                        TextBox_D.Text = D.ToString("F2");
                        TextBox_x.Visibility = Visibility.Hidden;
                        TextBox_x1.Visibility = Visibility.Hidden;
                        TextBox_x2.Visibility = Visibility.Hidden;
                        label_x.Visibility = Visibility.Hidden;
                        label_x1.Visibility = Visibility.Hidden;
                        label_x2.Visibility = Visibility.Hidden;
                        MessageBox.Show("Рівняння немає коренів!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    else
                    {
                        TextBox_D.Text = D.ToString("F2");
                        double x = x1 = x2;
                        TextBox_x1.Visibility = Visibility.Hidden;
                        TextBox_x2.Visibility = Visibility.Hidden;
                        label_x1.Visibility = Visibility.Hidden;
                        label_x2.Visibility = Visibility.Hidden;
                        label_x.Visibility = Visibility.Visible;
                        TextBox_x.Visibility = Visibility.Visible;
                        TextBox_x.Text = x.ToString("F2");
                        return;
                    }
                }
            } while (true);
        }

        private void TextBox_a_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ".")
               && (!TextBox_a.Text.Contains(".")
               && TextBox_a.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }

        private void TextBox_b_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ".")
               && (!TextBox_b.Text.Contains(".")
               && TextBox_b.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }

        private void TextBox_c_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ".")
               && (!TextBox_c.Text.Contains(".")
               && TextBox_c.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }
    }
}